# Setup
Application consist of 4 services

- applications
- servers
- auth-management
- users

In this test scenario we create a relation between `server` and `appplication` (application runs on server).
To achieve this `application` model has `serverId` field that is populated by `populate` hook.

I have a hook defined in `/hooks/relation.js` as follows:
```
const { populate } = require('feathers-hooks-common');

module.exports = function (service, includedService, nameAs) {

  const options = {
    schema: {
      service, // applications
      include: {
        service: includedService, // servers 
        nameAs, // server
        parentField: `${nameAs}Id`, // serverId
        childField: '_id',
      },
    }
  };

  // this line does not throw
  if (Object.hasOwnProperty.call(options.schema.include, 'provider')) {
    throw new Error(`[RELATION-INIT] provider in schema.include with value ${options.schema.include.provider}`);
  }

  return context => {
    console.log('relation options.schema.include', options.schema.include);

    // this line throws !
    if (Object.hasOwnProperty.call(options.schema.include, 'provider')) {
      throw new Error(`[RELATION-CALL] provider in schema.include with value ${options.schema.include.provider}`);
    }

    return populate(options)(context);
  };
};
``` 

The above hook is used in `applications.hooks.js`:

```
const relation = require('../../hooks/relation');

before: {
  get: [
      restrictToOwner(),
  ],
}
...
after: {
  get: [
      context => console.log('applications.get provider', context.params.provider),
      relation('applications', 'servers', 'server'),
      context => console.log('applications.get result', context.result),
    ],
}
```

`servers.hooks.js` include extra logging: 
```
after: {
    all: [
      context => console.log('servers.' + context.method, 'provider', context.params.provider),
      context => console.log('servers.' + context.method, 'result', context.result),
    ],
}
```

# The problem

In `relation.js` hook somehow `options.schema.include.provider` field gets injected into the options const during service call. 
`provider` field is not present when the hook is initialized, but it magically shows up at execution stage.

Applications GET service call produce the following output:
```
applications.get provider undefined     <---- internal call from restrictToOwner starts
relation options.schema.include { service: 'servers',
  nameAs: 'server',
  parentField: 'serverId',
  childField: '_id' }                   <---- no provider injected
servers.find provider undefined        
servers.find result [ { _id: 5b353d8f973d3a1cacee984e, <------ result for populate
    name: 'Serveer 1',
    host: 'localhost',
    username: 'root',
    password: 'root',
    port: 4444,
    userId: 5b353d8b973d3a1cacee984d,
    createdAt: 2018-06-28T19:57:03.884Z,
    updatedAt: 2018-06-28T19:57:03.884Z,
    __v: 0 } ]
applications.get result { _id: 5b353d90973d3a1cacee9850, <------- result for restrictToOwner
  name: 'Serveer 1',
  serverId: 5b353d8f973d3a1cacee984e,
  port: 8400,
  userId: 5b353d8b973d3a1cacee984d,
  createdAt: 2018-06-28T19:57:04.692Z,
  updatedAt: 2018-06-28T19:57:04.692Z,
  __v: 0,
  _include: [ 'server' ],
  server: 
   { _id: 5b353d8f973d3a1cacee984e,
     name: 'Serveer 1',
     host: 'localhost',
     username: 'root',
     password: 'root',
     port: 4444,
     userId: 5b353d8b973d3a1cacee984d,
     createdAt: 2018-06-28T19:57:03.884Z,
     updatedAt: 2018-06-28T19:57:03.884Z,
     __v: 0 } }
applications.get provider rest          <------ actual external service call
relation options.schema.include { service: 'servers',
  nameAs: 'server',
  parentField: 'serverId',
  childField: '_id',
  provider: undefined }         <-------- undefined provider injected = volunerability !
error:  Error: [RELATION-CALL] provider in schema.include with value undefined
    at Object.context (E:/Dev/node.js/feathers-populate-test/src/hooks/relation.js:28:13)
    at <anonymous>
    at process._tickCallback (internal/process/next_tick.js:188:7)
```

But there's more... When you comment out `applications.hooks.js`: 

```
before: {
  get: [
      // restrictToOwner(),
  ],
}
```

a `rest` provider is injected!

```
applications.get provider rest        <------- external service called directly
relation options.schema.include { service: 'servers',
  nameAs: 'server',
  parentField: 'serverId',
  childField: '_id',
  provider: 'rest' }                 <------- rest provider injected !
error:  Error: [RELATION-CALL] provider in schema.include with value rest
    at Object.context (E:/Dev/node.js/feathers-populate-test/src/hooks/relation.js:28:13)
    at <anonymous>
    at process._tickCallback (internal/process/next_tick.js:188:7)
```

# How to run

- You need to have a MongoDB installed and accessible on `localhost:27017`
- `npm install`
- `npm mocha`
- Drop `antara-cluster-test` database before next test run


# Solution 

Pipelines results tested on node:8.11.3 and node:10.5.0:

**Failing (without populate fix):**

[https://bitbucket.org/AntaraSI/feathers-populate-test/addon/pipelines/home#!/results/5](https://bitbucket.org/AntaraSI/feathers-populate-test/addon/pipelines/home#!/results/5)

**Successful (fixed):**

[https://bitbucket.org/AntaraSI/feathers-populate-test/addon/pipelines/home#!/results/6](https://bitbucket.org/AntaraSI/feathers-populate-test/addon/pipelines/home#!/results/6)
