/* eslint-disable no-console */

const Promise = require('bluebird');

function serverListen(app) {
  const port = app.get('port');

  return app.listen(port);
}

module.exports = function createServer(app) {
  if (process.env.PORT) {
    app.set('port', process.env.PORT);
  }

  return new Promise((resolve) => {
    const server = serverListen(app);

    server.once('listening', () => {
      resolve(server);
    });
  });
};
