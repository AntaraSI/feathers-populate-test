// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

const { populate } = require('feathers-hooks-common');

module.exports = function (service, includedService, nameAs) {

  const options = {
    schema: {
      service,
      include: {
        service: includedService,
        nameAs,
        parentField: `${nameAs}Id`,
        childField: '_id',
      },
    }
  };

  if (Object.hasOwnProperty.call(options.schema.include, 'provider')) {
    throw new Error(`[RELATION-INIT] provider in schema.include with value ${options.schema.include.provider}`);
  }

  return context => {
    console.log('relation options.schema.include', options.schema.include);

    if (Object.hasOwnProperty.call(options.schema.include, 'provider')) {
      throw new Error(`[RELATION-CALL] provider in schema.include with value ${options.schema.include.provider}`);
    }

    return populate(options)(context);
  };
};
