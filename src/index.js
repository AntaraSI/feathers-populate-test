/* eslint-disable no-console */
const logger = require('winston');
const app = require('./app');
const createServer = require('./create-server');

process.on('unhandledRejection', (reason, p) => {
  logger.error('Unhandled Rejection at: Promise ', p, reason);

  if (p.hook) {
    logger.error('hook params', p.hook.params);
    logger.error('hook result', p.hook.result);
  }

  if (Array.isArray(p.stackframes)) {
    for (let s of p.stackframes) {
      logger.error('stackframes[]', s);
    }
  }
});

createServer(app).then(() => {
  logger.info('Feathers application started on %s://%s:%d', app.get('protocol'), app.get('host'), app.get('port'));
});
