// applications-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const ObjectId = Schema.Types.ObjectId;

  const applications = new Schema({
    name: { type: String, required: true },
    port: { type: Number, min: 1, max: 65535 },
    serverId: { type: ObjectId, required: true, index: true },
    userId: { type: ObjectId, required: true, index: true },
  }, {
    timestamps: true
  });

  return mongooseClient.model('applications', applications);
};
