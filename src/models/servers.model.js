// servers-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const ObjectId = Schema.Types.ObjectId;

  const servers = new Schema({
    name: { type: String, required: true },
    host: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String },
    port: { type: Number, min: 0, max: 65535 },
    userId: { type: ObjectId, index: true, required: true },
  }, {
    timestamps: true
  });

  return mongooseClient.model('servers', servers);
};
