// users-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const users = new Schema({
    email: { type: String, required: true, index: true, unique: true, uniqueCaseInsensitive: true },
    password: { type: String, required: true },
    isVerified: { type: Boolean },
    verifyToken: { type: String },
    verifyShortToken: { type: String },
    verifyExpires: { type: Date }, // or a long integer
    verifyChanges: { type: Array },
    resetToken: { type: String },
    resetShortToken: { type: String },
    resetExpires: { type: Date }, // or a long integer
  }, {
    timestamps: true
  });

  return mongooseClient.model('users', users);
};
