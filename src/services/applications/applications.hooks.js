const {authenticate} = require('@feathersjs/authentication/lib/hooks');
const { restrictToOwner, associateCurrentUser } = require('feathers-authentication-hooks');
const { disallow } = require('feathers-hooks-common');

const relation = require('../../hooks/relation');

module.exports = {
  before: {
    all: [
      authenticate('jwt'),
    ],
    find: [
      // restrictToOwner(),
    ],
    get: [
      restrictToOwner(),
    ],
    create: [
      associateCurrentUser(),
    ],
    update: [
      disallow(),
    ],
    patch: [
      restrictToOwner(),
    ],
    remove: [
      restrictToOwner(),
    ]
  },

  after: {
    all: [],
    find: [
      context => console.log('applications.find provider', context.params.provider),
      relation('applications', 'servers', 'server'),
      context => console.log('applications.find result', context.result),
    ],
    get: [
      context => console.log('applications.get provider', context.params.provider),
      relation('applications', 'servers', 'server'),
      context => console.log('applications.get result', context.result),
    ],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
