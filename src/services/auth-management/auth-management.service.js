const feathersAuthManagement = require('feathers-authentication-management');
const hooks = require('./auth-management.hooks');

module.exports = function (app) {

  const config = app.get('auth-management');

  app.configure(feathersAuthManagement(config));

  const service = app.service(config.path);

  service.hooks(hooks);
};
