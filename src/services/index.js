const users = require('./users/users.service.js');
const authManagement = require('./auth-management/auth-management.service.js');
const servers = require('./servers/servers.service.js');
const applications = require('./applications/applications.service.js');
module.exports = function (app) {
  app.configure(users);
  app.configure(authManagement);
  app.configure(servers);
  app.configure(applications);
};
