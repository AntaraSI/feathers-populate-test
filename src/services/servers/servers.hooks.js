const {authenticate} = require('@feathersjs/authentication/lib/hooks');
const { associateCurrentUser, restrictToOwner } = require('feathers-authentication-hooks');
const { disallow } = require('feathers-hooks-common');

module.exports = {
  before: {
    all: [
      authenticate('jwt'),
    ],
    find: [
      restrictToOwner(),
    ],
    get: [
      restrictToOwner(),
    ],
    create: [
      associateCurrentUser(),
    ],
    update: [
      disallow(),
    ],
    patch: [
      restrictToOwner(),
    ],
    remove: [
      restrictToOwner(),
    ]
  },

  after: {
    all: [
      context => console.log('servers.' + context.method, 'provider', context.params.provider),
      context => console.log('servers.' + context.method, 'result', context.result),
    ],
    find: [
    ],
    get: [
    ],
    create: [
    ],
    update: [
      // disallowed
    ],
    patch: [
    ],
    remove: [
    ]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
