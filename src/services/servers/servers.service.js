// Initializes the `servers` service on path `/servers`
const createService = require('feathers-mongoose');
const createModel = require('../../models/servers.model');
const hooks = require('./servers.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'servers',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/servers', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('servers');

  service.hooks(hooks);
};
