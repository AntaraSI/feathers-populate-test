const { addVerification, removeVerification } = require('feathers-authentication-management').hooks;
const { iff, isProvider, preventChanges, disallow } = require('feathers-hooks-common');
const { hashPassword, protect } = require('@feathersjs/authentication-local').hooks;
const {authenticate} = require('@feathersjs/authentication/lib/hooks');

module.exports = {
  before: {
    all: [],
    find: [
      authenticate('jwt'),
    ],
    get: [
      authenticate('jwt'),
    ],
    create: [
      // everyone can create an account
      addVerification('auth-management'),
      hashPassword(),
    ],
    update: [
      disallow(),
    ],
    patch: [
      authenticate('jwt'),
      iff(isProvider('external'), preventChanges(true,
        '_id',
        'email',
        'isVerified',
        'verifyToken',
        'verifyShortToken',
        'verifyExpires',
        'verifyChanges',
        'resetToken',
        'resetShortToken',
        'resetExpires'
      )),
      hashPassword(),
    ],
    remove: [
      // don't allow user removal //TODO soft delete
      disallow('external'),
    ]
  },

  after: {
    all: [
      // removes verification/reset fields other than .isVerified
      removeVerification(),
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password'),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
