import {serverInstance, serverCfg} from './server.cfg';

export const appCfg = [
  {
    name: serverCfg[0].name,
    get serverId() {
      return serverInstance[0]._id;
    },
    port: 8400,
  },
  {
    name: serverCfg[1].name,
    get serverId() {
      return serverInstance[1]._id;
    },
    port: 8600,
  },
];

export const appInstance = appCfg.map(() => ({
  _id: null,
}));
