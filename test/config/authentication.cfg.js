import rp from 'request-promise';
import {getUrl} from '../server.startup';

export const authenticationCfg = {
  email: 'user@domain.com',
  password: 'user@domain.com',
};

function internalGetAccessToken({email, password}) {
  return rp({
    url: getUrl('authentication'),
    method: 'POST',
    json: true,
    body: {
      strategy: 'local',
      email,
      password,
    }
  }).then(res => {
    return res.accessToken;
  });
}

export function getAccessToken() {
  return internalGetAccessToken(authenticationCfg);
}
