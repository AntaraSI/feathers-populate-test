export const serverCfg = [
  {
    name: 'Serveer 1',
    host: 'localhost',
    username: 'root',
    password: 'root',
    port: 4444,
  },
  {
    name: 'Server 2',
    host: 'localhost',
    username: 'root',
    password: 'root',
    port: 6666,
  },
];

export const serverInstance = serverCfg.map(() => ({
  _id: null,
}));
