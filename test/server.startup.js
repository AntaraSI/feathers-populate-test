export const app = require('../src/app');
const createServer = require('../src/create-server');
const url = require('url');

const chai = require('chai');
chai.use(require('chai-as-promised'));
chai.use(require('chai-string'));

export const should = chai.should();
export const expect = chai.expect;

const port = app.get('port') || 3030;

export const getUrl = (pathname, search) => url.format({
  hostname: app.get('host') || 'localhost',
  protocol: app.get('protocol') || 'http',
  port,
  pathname,
  search,
});

export let server;

export async function awaitServerListening() {
  if (!server) {
    server = await createServer(app);
  }
}
