import rp from 'request-promise';
import {awaitServerListening, getUrl, app} from '../server.startup';
import assert from 'assert';
import {authenticationCfg} from '../config/authentication.cfg';
const {email, password} = authenticationCfg;

describe('\'auth-management\' service', () => {
  before(awaitServerListening);

  it('registered the service', () => {
    const service = app.service('auth-management');

    assert.ok(service, 'Registered the service');
  });

  describe(`user ${email}`, () => {
    it('register', async () => {
      const res = await rp({
        url: getUrl('users'),
        method: 'POST',
        json: true,
        body: {
          email,
          password,
        }
      });

      res.should.be.an('object').that.have.all.keys('__v', '_id', 'email', 'isVerified', 'createdAt', 'updatedAt');
    });
  });
});
