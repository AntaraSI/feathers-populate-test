import {awaitServerListening, app} from '../server.startup';
import assert from 'assert';
import {getAccessToken} from '../config/authentication.cfg';

describe('\'users\' service', () => {
  before(awaitServerListening);

  it('registered the service', () => {
    const service = app.service('users');

    assert.ok(service, 'Registered the service');
  });

  it('loggs in', async () => {
    const accessToken = await getAccessToken();

    accessToken.should.be.a('string');
    accessToken.split('.').should.have.lengthOf(3); // probably valid JWT token
  });
});
