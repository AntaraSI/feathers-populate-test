import rp from 'request-promise';
import {awaitServerListening, getUrl, app} from '../server.startup';
import assert from 'assert';
import {serverCfg, serverInstance} from '../config/server.cfg';
import {getAccessToken} from '../config/authentication.cfg';

describe('\'servers\' service', () => {
  before(awaitServerListening);

  it('registered the service', () => {
    const service = app.service('servers');

    assert.ok(service, 'Registered the service');
  });

  for (let i = 0; i < serverCfg.length; i++) {
    const server = serverCfg[i];
    const instance = serverInstance[i];

    describe(`server ${server.name}`, () => {
      it('create new server', async () => {
        const authorization = await getAccessToken();

        const res = await rp({
          url: getUrl('/servers'),
          headers: {
            authorization,
          },
          method: 'POST',
          json: true,
          body: server,
        });

        Object.keys(res).forEach(key => {
          instance[key] = res[key];
        });

        res.should.include(server);
        res.should.have.property('userId');
      });
    });
  }
});
