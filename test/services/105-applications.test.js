import {serverCfg} from '../config/server.cfg';
import {appInstance, appCfg} from '../config/application.cfg';
import rp from 'request-promise';
import {getUrl, app} from '../server.startup';
const assert = require('assert');
import {getAccessToken} from '../config/authentication.cfg';

describe('\'applications\' service', () => {
  it('registered the service', () => {
    const service = app.service('applications');

    assert.ok(service, 'Registered the service');
  });

  for (let i = 0; i < serverCfg.length; i++) {
    const server = serverCfg[i];
    const app = appCfg[i];

    describe(`application for ${server.name}`, () => {
      it('create', async () => {
        const authorization = await getAccessToken();

        const res = await rp({
          url: getUrl('/applications'),
          headers: {authorization},
          method: 'POST',
          json: true,
          body: app,
        });

        Object.keys(res).forEach(key => {
          appInstance[i][key] = res[key];
        });

        res.should.include(app);
        res.should.have.property('userId');
      });
    });
  }
});
