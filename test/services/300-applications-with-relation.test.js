import rp from 'request-promise';
import {awaitServerListening, getUrl, app} from '../server.startup';
import assert from 'assert';
import {serverCfg} from '../config/server.cfg';
import {getAccessToken} from '../config/authentication.cfg';
import {appInstance, appCfg} from '../config/application.cfg';

describe('applications with relation', () => {
  before(awaitServerListening);

  it('registered the service', () => {
    const service = app.service('applications');
    assert.ok(service, 'Registered the service');
  });

  describe('get apps with server', () => {
    for (let i = 0; i < serverCfg.length; i++) {
      const server = serverCfg[i];
      const app = appInstance[i];

      it(`get ${appCfg[i].name} app`, async () => {
        const authorization = await getAccessToken();

        const res = await rp({
          url: getUrl(`/applications/${app._id}`),
          method: 'GET',
          json: true,
          headers: {authorization},
        });

        res.should.include(app);
        res.should.have.property('server').include(server);
      });
    }
  });

});
